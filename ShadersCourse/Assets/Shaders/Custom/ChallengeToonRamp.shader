﻿Shader "Custom/ToonRamp" {
	Properties {
		_Colour("Colour", Color) = (1,1,1,1)
		_RampTex ("Ramp Texture", 2D) = "white" {}
	}

	SubShader {
		
		CGPROGRAM
		#pragma surface surf ToonRamp

		float4 _Colour;
		sampler2D _RampTex;

		half4 LightingToonRamp(SurfaceOutput s, fixed3 lightDir, half3 viewDir, fixed atten) {
			float diff = dot(s.Normal, lightDir);
			half3 h = normalize(lightDir + viewDir) * diff * .5 + .5;
			//float h = diff * .5 + .5;
			float2 rh = h;
			float3 ramp = tex2D(_RampTex, rh).rgb;

			float4 c;
			c.rgb = s.Albedo * _LightColor0.rgb * (ramp);
			c.a = s.Alpha;

			return c;
		}

	
		

        struct Input {
            float2 uv_MainTex;
			float2 uv_ImgTex;
        };

        
        void surf (Input IN, inout SurfaceOutput o)  {
			//o.Albedo = _Colour.rgb;
			//o.Albedo = (1, 0, 1, 1);
			o.Albedo = tex2D(_RampTex, IN.uv_ImgTex).rgb;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
