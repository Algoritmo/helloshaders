﻿Shader "Custom/MyBumpedEnvmnt" {
    Properties  {
        _myNormalMap( "Normal Map", 2D) = "bump" {}
		_myCube("Cube Map", CUBE) = "white" {}
	}

		SubShader{

			CGPROGRAM
			#pragma surface surf Lambert

			sampler2D _myNormalMap;
			samplerCUBE _myCube;
        
			struct Input {
				float2 uv_myNormals;
				float3 worldRefl; INTERNAL_DATA
			};

			void surf (Input IN, inout SurfaceOutput o) {
				o.Normal = UnpackNormal(tex2D(_myNormalMap, IN.uv_myNormals)) * .3;
				o.Albedo = texCUBE(_myCube, WorldReflectionVector(IN, o.Normal)).rgb;
			}
        ENDCG
    }
    FallBack "Diffuse"
}
