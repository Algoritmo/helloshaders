﻿Shader "Custom/MyShader"
{
    Properties
    {
        _myColor ("Color", Color) = (1,1,1,1)
		_myTex("Example Texture", 2D) = "white" {}
	}
		SubShader
	{

		CGPROGRAM
		#pragma surface surf Lambert

		fixed4 _myColor;
        sampler2D _myTex;

        struct Input {
			float2 uv_myTex;
        };

                
        void surf (Input IN, inout SurfaceOutput o) {
			float3 _aux = tex2D(_myTex, IN.uv_myTex).rgb;
			_aux.g = 1;
			o.Albedo.rgb = _aux;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
