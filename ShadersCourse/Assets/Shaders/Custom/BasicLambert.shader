﻿Shader "Custom/BasicLambert" {
	Properties{
		_myColour("Colour", Color) = (1, 1, 1, 1)
		
	}

		SubShader{
			Tags{
				"Queue" = "Geometry"
			}

			CGPROGRAM
			#pragma surface surf BlinnPhong

			float4 _myColour;
			

        struct Input  {
			float2 uv_MainTex;
        };

               
        void surf (Input IN, inout SurfaceOutput o)  {
			o.Albedo = _myColour.rgb;
			
        }
        ENDCG
    }
    FallBack "Diffuse"
}
