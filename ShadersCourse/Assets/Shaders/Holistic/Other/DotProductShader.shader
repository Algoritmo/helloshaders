﻿Shader "Holistic/DotProductShader" {

    SubShader {
        
        CGPROGRAM
        #pragma surface surf Lambert

        
        struct Input {
			float3 viewDir;
        };

        

        void surf (Input IN, inout SurfaceOutput o) {
			half dotP = dot(IN.viewDir, o.Normal);
			o.Albedo = float3(dotP, .9, .9);
        }
        ENDCG
    }
    FallBack "Diffuse"
}
