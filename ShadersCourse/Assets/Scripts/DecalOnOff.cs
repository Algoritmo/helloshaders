﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[ExecuteInEditMode]
public class DecalOnOff : MonoBehaviour {
    Material mat;
    bool showDecal = false;

    
    private void Start() {
        mat = this.GetComponent<Renderer>().sharedMaterial;
    }


    private void OnMouseDown() {
        Debug.Log("clicked");
        showDecal = !showDecal;

        if (showDecal)
            mat.SetFloat("_ShowDecal", 1);
        else
            mat.SetFloat("_ShowDecal", 0);
    }
}
